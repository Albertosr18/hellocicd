# CI/CD Application

This is a project to test continuous integration and delivery with two of the most famous alternatives as <b>Jenkins</b> and <b>Gitlab CI</b>.

## Pre-requisites
* Install Jenkins (https://jenkins.io/doc/book/installing/#war-file)
* Google Cloud account
* SDK Cloud (https://cloud.google.com/sdk/?hl=es)
* Gitlab account
* Docker installed
* Maven installed
* Fork this project

## Configure Jenkins

* Add https://gitlab.com/sevtech/courses/cicd/shared-libraries.git to shared libraries.

## Google Cloud configuration

* Create service account
```sh  
gcloud iam service-accounts create [SA-NAME] --display-name "[SA-DISPLAY-NAME]" 
```

* Create service account key
```sh  
gcloud iam service-accounts keys create key.json --iam-account [SA-NAME]@[PROJECT-ID].iam.gserviceaccount.com
```

## Authors

Miguel Ángel Quintanilla y Francisco Javier Delgado Vallano.